#include <stdio.h>
int main()
{
    int A=10, B=15;
    printf("Output of %d & %d is %d\n",A,B,A&B);
    printf("Output of %d ^ %d is %d\n",A,B,A^B);
    printf("Output of ~ %d is %d\n",A,~A);
    printf("Output of %d<<3 is %d\n",A,A<<3);
    printf("Output of %d>>3 is %d\n",B,B>>3);
    return 0;
}
